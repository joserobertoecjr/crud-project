import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Aluno } from '../aluno';
import { ALUNOS } from '../mock-alunos';

@Component({
  selector: 'app-alunos',
  templateUrl: './alunos.component.html',
  styleUrls: ['./alunos.component.css']
})
export class AlunosComponent implements OnInit {

  @ViewChild('novoAluno') novoAluno: ElementRef;

  idNumber: number;

  alunos = ALUNOS;
  selectedAluno: Aluno;

  constructor() {
    this.idNumber = 10;
  }

  ngOnInit() {
  }

  onSelect(aluno: Aluno):void {
    this.selectedAluno = aluno;
    console.log(aluno)
  }

  deleteAluno(aluno){
    var index = this.alunos.indexOf(aluno);
    this.alunos.splice(index, 1);
  }

  addAluno(name){
    this.alunos.push({id: this.idNumber++, name: name});
    this.novoAluno.nativeElement.value = '';
  }
}
